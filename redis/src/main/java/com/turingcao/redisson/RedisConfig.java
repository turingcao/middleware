package com.turingcao.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.api.RedissonReactiveClient;
import org.redisson.api.RedissonRxClient;
import org.redisson.config.Config;

import java.io.File;
import java.io.IOException;

public class RedisConfig {

    public Config getConfig() throws IOException {
        Config config = new Config();
        config.useClusterServers()
                // use "rediss://" for SSL connection
                .addNodeAddress("redis://127.0.0.1:7181");

// or read config from file
        config = Config.fromYAML(new File("config-file.yaml"));
        return config;
    }

    public Redisson getRedisson(Config config) {
// Sync and Async API
        RedissonClient redisson = Redisson.create(config);

// Reactive API
        RedissonReactiveClient redissonReactive = redisson.reactive();

// RxJava3 API
        RedissonRxClient redissonRx = redisson.rxJava();

        return null;
    }

}
